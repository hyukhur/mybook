//
//  EndPointTests.swift
//  MyBookTests
//
//  Created by hyukhur on 2022/02/13.
//

import XCTest
@testable import MyBook

class EndPointTests: XCTestCase {
    override func setUpWithError() throws {
        EndPoint.baseURL = URLComponents().url!
    }

    func testBaseURL() {
        EndPoint.baseURL = URL(string: "http://domain.com.some")!
        XCTAssertEqual(EndPoint.books(isbn13: "isbn").url.absoluteString, "http://domain.com.some/books/isbn")
    }

    func testGenerateBookURL() {
        let endPoint = EndPoint.books(isbn13: "1234567890123")
        XCTAssertEqual(endPoint.url.absoluteString, "books/1234567890123")
    }

    func testGenerateSearchURLForInitialState() {
        let endPoint = EndPoint.search(query: "", page: "")
        XCTAssertEqual(endPoint.url.absoluteString, "search")
    }

    func testGenerateSearchURLInSearching() {
        let endPoint = EndPoint.search(query: "keyword", page: "")
        XCTAssertEqual(endPoint.url.absoluteString, "search/keyword")
    }

    func testGenerateSearchURLInPaging() {
        let endPoint = EndPoint.search(query: "keyword", page: "2")
        XCTAssertEqual(endPoint.url.absoluteString, "search/keyword/2")
    }

    func testGenerateSearchURLWithDefaultPage() {
        let endPoint = EndPoint.search(query: "keyword")
        XCTAssertEqual(endPoint.url.absoluteString, "search/keyword/1")
        XCTAssertEqual(EndPoint.search(query: "").url.absoluteString, "search")
    }
}
