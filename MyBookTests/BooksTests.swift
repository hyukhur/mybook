//
//  BooksTests.swift
//  MyBookTests
//
//  Created by hyukhur on 2022/02/13.
//

import XCTest
@testable import MyBook

class BooksTests: XCTestCase {
    static func makeBook() -> Book {
        Book(isbn13: "isbn13_\(UUID().uuidString)", title: "title", subtitle: "subtitle", price: "price", image: URLComponents().url!, url: URLComponents().url!)
    }
    lazy var mockBooks: [Book] = (0..<71).map({ _ in Self.makeBook() })

    func testBooks() {
        let books = Books(total: "2", page: "1", books: [Self.makeBook(), Self.makeBook()])
        XCTAssertEqual(books.books.count, 2)
        XCTAssertEqual(books.total, "2")
        XCTAssertEqual(books.page, "1")
        XCTAssertEqual(books.nextPage, "2")
    }

    func testNextPage() {
        let head = Books(total: "71", page: "1", books: mockBooks[0..<10].map({ $0 }))
        XCTAssertEqual(head.nextPage, "2")
        XCTAssertEqual(Books(total: "71", page: "2", books: mockBooks[10..<20].map({ $0 })).nextPage, "3")
    }

    func testNextPageEnded() {
        let negative = Books(total: "0", page: "-1", books: [Book]())
        XCTAssertEqual(negative.nextPage, "1")

        let underflow = Books(total: "71", page: "0", books: mockBooks[0..<10].map({ $0 }))
        XCTAssertEqual(underflow.nextPage, "2")

        let first = Books(total: "71", page: "1", books: mockBooks[0..<10].map({ $0 }))
        XCTAssertEqual(first.nextPage, "2")

        let second = Books(total: "71", page: "1", books: mockBooks[0..<20].map({ $0 }))
        XCTAssertEqual(second.nextPage, "2")

        let beforeLast = Books(total: "71", page: "7", books: mockBooks[0..<70].map({ $0 }))
        XCTAssertEqual(beforeLast.nextPage, "8")

        // https://api.itbook.store/1.0/search/mongo/8
        // returns
        // {"error":"0","total":"71","page":"8","books":[{"title":"Seven Databases in Seven Weeks","subtitle":"A Guide to Modern Databases and the NoSQL Movement","isbn13":"9781934356920","price":"$12.59","image":"https://itbook.store/img/books/9781934356920.png","url":"https://itbook.store/books/9781934356920"}]}
        let last = Books(total: "71", page: "8", books: mockBooks)
        XCTAssertEqual(last.nextPage, "9")

        let overflow = Books(total: "0", page: "9", books: [Book]())
        XCTAssertEqual(overflow.nextPage, "9")
    }

    func testStringInBooksTotal() {
        let NaN = Books(total: "Not a number", page: "0", books: [Book]())
        XCTAssertEqual(NaN.nextPage, "1")
    }

    func testStringInBooksPage() {
        let NaN = Books(total: "0", page: "Not a number", books: [Book]())
        XCTAssertEqual(NaN.nextPage, "1")
    }

    func testEmptyStringInBooksTotal() {
        let empty = Books(total: "", page: "0", books: [Book]())
        XCTAssertEqual(empty.nextPage, "1")
    }

    func testEmptyStringInBooksPage() {
        let empty = Books(total: "0", page: "", books: [Book]())
        XCTAssertEqual(empty.nextPage, "1")
    }

    func testMergeBooksInFirstParts() {
        let firstMerge = Books(total: "71", page: "2", books: mockBooks[10..<20].map({ $0 }))
            .merged(with: Books(total: "71", page: "1", books: mockBooks[0..<10].map({ $0 })))
        XCTAssertEqual(firstMerge.books.count, 20)
        XCTAssertEqual(firstMerge.books.first, mockBooks[0])
        XCTAssertEqual(firstMerge.books.last, mockBooks[19])
        XCTAssertEqual(firstMerge.nextPage, "3")
    }

    func testMergeBooksInReversed() {
        let reverseMerge = Books(total: "71", page: "1", books: mockBooks[0..<10].map({ $0 }))
            .merged(with: Books(total: "71", page: "2", books: mockBooks[10..<20].map({ $0 })))
        XCTAssertEqual(reverseMerge.books.count, 20)
        XCTAssertEqual(reverseMerge.books.first, mockBooks[0])
        XCTAssertEqual(reverseMerge.books.last, mockBooks[19])
        XCTAssertEqual(reverseMerge.nextPage, "3")
    }

    func testMergeBooksInSecondParts() {
        let merged = Books(total: "71", page: "3", books: mockBooks[20..<30].map({ $0 }))
            .merged(with: Books(total: "71", page: "2", books: mockBooks[0..<20].map({ $0 })))
        XCTAssertEqual(merged.books.count, 30)
        XCTAssertEqual(merged.books.first, mockBooks[0])
        XCTAssertEqual(merged.books.last, mockBooks[29])
        XCTAssertEqual(merged.nextPage, "4")
    }

    func testMergeBooksInBeforeLastParts() {
        let merged = Books(total: "71", page: "8", books: mockBooks[70..<71].map({ $0 }))
            .merged(with: Books(total: "71", page: "7", books: mockBooks[0..<70].map({ $0 })))
        XCTAssertEqual(merged.books.count, 71)
        XCTAssertEqual(merged.books.first, mockBooks.first)
        XCTAssertEqual(merged.books.last, mockBooks.last)
        XCTAssertEqual(merged.nextPage, "9")
    }

    func testMergeBooksInLastParts() {
        let merged = Books(total: "0", page: "9", books: [Book]())
            .merged(with: Books(total: "71", page: "8", books: mockBooks))
        XCTAssertEqual(merged.books.count, 71)
        XCTAssertEqual(merged.books.first, mockBooks[0])
        XCTAssertEqual(merged.books.last, mockBooks[70])
        XCTAssertEqual(merged.nextPage, "9")
    }

    func testMergeReversedBooksInLastParts() {
        let merged = Books(total: "71", page: "8", books: mockBooks)
            .merged(with: Books(total: "0", page: "9", books: [Book]()))
        XCTAssertEqual(merged.books.count, 71)
        XCTAssertEqual(merged.books.first, mockBooks[0])
        XCTAssertEqual(merged.books.last, mockBooks[70])
        XCTAssertEqual(merged.nextPage, "9")
    }

    func testMergeBooksInEmpty() {
        let merged = Books(total: "0", page: "9", books: [Book]())
            .merged(with: Books(total: "0", page: "-1", books: [Book]()))
        XCTAssertEqual(merged.books.count, 0)
        XCTAssertEqual(merged.nextPage, "1")
    }

    func testHasMore() {
        let books = Books(total: "2", page: "1", books: [Self.makeBook(), Self.makeBook()])
        XCTAssertEqual(books.books.count, 2)
        XCTAssertEqual(books.total, "2")
        XCTAssertEqual(books.page, "1")
        XCTAssertEqual(books.nextPage, "2")
        XCTAssertEqual(books.hasMore, false)

        let first = Books(total: "71", page: "1", books: mockBooks[0..<10].map({ $0 }))
        XCTAssertEqual(first.nextPage, "2")
        XCTAssertEqual(first.hasMore, true)

        let last = Books(total: "71", page: "8", books: mockBooks)
        XCTAssertEqual(last.nextPage, "9")
        XCTAssertEqual(last.hasMore, false)
    }
}
