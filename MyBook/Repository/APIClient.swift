//
//  APIClient.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/14.
//

import Foundation

enum APIResponseError: Error {
    case unknown
    case httpResponse(HTTPURLResponse)
}

protocol API {
    func get<T>(to: EndPoint, completion: @escaping (Result<T, Error>) -> Void) where T : Decodable
    // TODO: not yet implemented
    func post<T>(to: EndPoint, completion: @escaping (Result<T, Error>) -> Void) where T : Decodable
}

protocol URLManager {
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}

extension URLSession: URLManager {}

struct APIClient {
    // TODO: we could load data from a custom class if needed.
    var urlSession: URLManager
    var decoder: JSONDecoder

    init(urlSession: URLManager, decoder: JSONDecoder) {
        self.urlSession = urlSession
        self.decoder = decoder
    }

    static var shared: API = {
        let configuration = URLSessionConfiguration.default
        configuration.urlCache = URLCache()
        let session = URLSession(configuration: configuration)
        let apiClient = APIClient(urlSession: session, decoder: JSONDecoder())
        return apiClient
    }()
}

extension APIClient {
    func makeHttpURLResponseError(_ httpURLResponse: HTTPURLResponse) -> APIResponseError? {
        switch httpURLResponse.statusCode {
            case 200..<300:
                return nil
            case 303:
                //TODO: redirect handle
                return .httpResponse(httpURLResponse)
            case 401:
                //TODO: authentication handle
                return .httpResponse(httpURLResponse)
            case 500..<600:
                // TODO: break down handle
                return .httpResponse(httpURLResponse)
            default:
                return .httpResponse(httpURLResponse)
        }

    }
}

extension APIClient: API {
    func get<T>(to: EndPoint, completion: @escaping (Result<T, Error>) -> Void) where T : Decodable {
        let url = to.url
#if DEBUG
        print(url)
#endif
        urlSession.dataTask(with: url) {[makeHttpURLResponseError] data, urlResponse, error in
            guard let httpURLResponse = urlResponse as? HTTPURLResponse else {
                completion(.failure(APIResponseError.unknown))
                return
            }
            if let httpResponseError = makeHttpURLResponseError(httpURLResponse) {
                completion(.failure(httpResponseError))
                return
            }
            guard let data = data else {
                completion(.failure(APIResponseError.unknown))
                return
            }
            do {
                let decoded = try decoder.decode(T.self, from: data)
                completion(.success(decoded))
            } catch {
                // TODO: error handle
#if DEBUG
                print((try? JSONSerialization.jsonObject(with: data)) as Any)
#endif
                completion(.failure(error))
            }
        }
        .resume()
    }

    func post<T>(to: EndPoint, completion: @escaping (Result<T, Error>) -> Void) where T : Decodable {
        assertionFailure("not yet implemented")
    }
}
