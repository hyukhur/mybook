//
//  BookDetailCommentInputView.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/17.
//

import UIKit

class BookDetailCommentInputView: UITableViewHeaderFooterView {
    static let identifier = "BookDetailCommentInputView"

    let textField: UITextField = UITextField(frame: .zero)

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)

        textField.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        textField.returnKeyType = .done
        textField.borderStyle = .roundedRect
        textField.clearButtonMode = .whileEditing
        textField.frame = contentView.frame

        contentView.addSubview(textField)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
