//
//  APIClientTests.swift
//  MyBookTests
//
//  Created by hyukhur on 2022/02/14.
//

import XCTest
@testable import MyBook

private struct FakeURLManager: URLManager {
    var result: (Data?, URLResponse?, Error?)?
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        completionHandler(result?.0, result?.1, result?.2)
        return URLSession.shared.dataTask(with: URL(string: "localhost")!)
    }
}

private struct TestObject: Codable {
    let id: UUID
    let value: String
}

class APIClientTests: XCTestCase {
    fileprivate let testObject = TestObject(id: UUID(), value: "test value")
    fileprivate lazy var testData = try! JSONEncoder().encode(testObject)

    func testGetMethod() {
        let endPoint = EndPoint.books(isbn13: "isbn")
        var urlSession = FakeURLManager()
        urlSession.result = (testData, HTTPURLResponse(url: endPoint.url, statusCode: 200, httpVersion: "1.1", headerFields: nil), nil)
        let api = APIClient(urlSession: urlSession, decoder: JSONDecoder())

        api.get(to: endPoint) { (result: Result<TestObject, Error>) in
            XCTAssertEqual((try? result.get())?.value, "test value")
        }
    }

    func testGetWithError() {
        let endPoint = EndPoint.books(isbn13: "isbn")
        var urlSession = FakeURLManager()
        urlSession.result = (testData, HTTPURLResponse(url: endPoint.url, statusCode: 303, httpVersion: "1.1", headerFields: nil), nil)
        let api = APIClient(urlSession: urlSession, decoder: JSONDecoder())

        api.get(to: endPoint) { (result: Result<TestObject, Error>) in
            do {
                _ = try result.get()
                XCTFail()
            } catch APIResponseError.httpResponse(let HTTPURLResponse) {
                XCTAssertEqual(HTTPURLResponse.statusCode, 303)
            } catch {
                XCTFail()
            }
        }
    }
}
