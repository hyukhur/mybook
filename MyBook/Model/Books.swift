//
//  Books.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/13.
//

import Foundation

struct Books: Codable {
    let total: String
    let page: String
    let books: [Book]
}

extension Books {
    static let none: Books = Books(total: "", page: "", books: [])

    var nextPage: String {
        guard let totalNumber = Int(total),
              let pageNumber = Int(page) else {
                  return "1"
              }
        guard pageNumber > 0 else {
            return pageNumber < 0 ? "1" : "2"
        }
        guard totalNumber > 0 else {
            return page
        }
        guard totalNumber >= books.count else {
            return page
        }
        let nextPage = max(1, pageNumber + 1)
        return String(nextPage)
    }

    var hasMore: Bool {
        total != String(books.count)
    }

    func merged(with anotherBooks: Books) -> Books {
        guard total == anotherBooks.total else {
            return total > anotherBooks.total ? self : anotherBooks
        }
        
        let newPage = page > anotherBooks.page ? page : anotherBooks.page
        let newBooks = page > anotherBooks.page ? anotherBooks.books + books : books + anotherBooks.books

        if let totalNumber = Int(total), totalNumber > 0 {
            return Books(total: total, page: newPage , books: newBooks)
        } else {
            return Books(total: total, page: "1" , books: [])
        }
    }
}
