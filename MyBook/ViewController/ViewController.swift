//
//  ViewController.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/12.
//

import UIKit

class ViewController: UIViewController {
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: self.view.bounds)
        tableView.delegate = self
        tableView.dataSource = self
        // TODO: Make sure it's the latest way.
        tableView.register(BookTableViewCell.self, forCellReuseIdentifier: BookTableViewCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        return tableView
    }()

    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicatorView.sizeToFit()
        activityIndicatorView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl: UIRefreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "ViewController.refreshControl"))
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        return refreshControl
    }()

    lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.definesPresentationContext = true
        return searchController
    }()

    // TODO: Could be reduced model
    var books: Books = .none
    var searchKeyword: String = "" {
        didSet {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.refresh(_:)), object: #function)
            perform(#selector(self.refresh(_:)), with: #function, afterDelay: 0.3)
        }
    }

    let apiClient: API = APIClient.shared

    override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("Book Search", comment: "ViewController")
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        tableView.addSubview(refreshControl)
        view.addSubview(tableView)

        refresh(nil)

        DispatchQueue.main.async {
            self.tableView.tableFooterView = self.activityIndicatorView
        }
    }
}

extension ViewController {
    private func isNeedToPrefetch(at indexPath: IndexPath) -> Bool {
        indexPath.row > (books.books.count - 5)
    }

    @objc
    func refresh(_ sender: Any?) {
        guard !searchKeyword.isEmpty else { return }
        refreshControl.beginRefreshing()
        fetch { [weak self] in
#if DEBUG
            print($0)
#endif
            guard let `self` = self else { return }
            defer {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
            if case .success(let result) = $0 {
                self.books = result
            } else {
                self.books = .none
            }

            // TODO: show errors
        }
    }

    func loadMore() {
        guard !searchKeyword.isEmpty else { return }
        guard !activityIndicatorView.isAnimating else { return }
        guard books.hasMore else { return }
        activityIndicatorView.startAnimating()
        nextFetch {[weak self] in
#if DEBUG
            print($0)
#endif
            guard let `self` = self else { return }
            defer {
                DispatchQueue.main.async {
                    // TODO: insert only diff indexes
                    self.tableView.reloadData()
                    self.activityIndicatorView.stopAnimating()
                }
            }

            switch $0 {
                case .success(let books):
                    self.books = self.books.merged(with: books)
                default:
                    self.books = .none
            }

            // TODO: show errors
        }
    }
}

extension ViewController {
    func fetch(completion: @escaping (Result<Books, Error>) -> Void) {
        apiClient.get(to: EndPoint.search(query: searchKeyword), completion: completion)
    }

    func nextFetch(completion: @escaping (Result<Books, Error>) -> Void) {
        apiClient.get(to: EndPoint.search(query: searchKeyword, page: books.nextPage), completion: completion)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        books.books.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = books.books[indexPath.row]
        let detailViewController = DetailViewController()
        detailViewController.apiClient = apiClient
        detailViewController.isbn13 = book.isbn13
        detailViewController.title = NSLocalizedString("Detail Book", comment: "detailViewController.title")
        navigationController?.pushViewController(detailViewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // TODO: would be a factory method
        let cell = tableView.dequeueReusableCell(withIdentifier: BookTableViewCell.identifier, for: indexPath) as? BookTableViewCell ?? BookTableViewCell(style: .default, reuseIdentifier: BookTableViewCell.identifier)
        cell.book = books.books[indexPath.row] // TODO: will be safe a subscript.
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if isNeedToPrefetch(at: indexPath) {
            loadMore()
        }
    }
}

extension ViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searching = searchController.searchBar.text, !searching.isEmpty else {
            books = .none
            tableView.reloadData()
            return
        }
        searchKeyword = searching
    }
}
