//
//  Book.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/13.
//

import Foundation

struct Book: Codable, Equatable {
    let isbn13: String

    let title: String
    let subtitle: String
    let price: String
    let image: URL
    let url: URL

    var id: String {
        isbn13
    }

    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.id == rhs.id
    }
}
