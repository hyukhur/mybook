//
//  Comment.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/17.
//

import Foundation

struct Comment: Codable {
    let uuid: UUID
    let content: String
}
