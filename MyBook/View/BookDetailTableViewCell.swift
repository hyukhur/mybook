//
//  BookDetailTableViewCell.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/12.
//

import UIKit

class BookDetailTableViewCell: UITableViewCell {
    static let identifier: String = "BookDetailTableViewCell"

    var comment: Comment? {
        didSet {
            guard let comment = comment else {
                textLabel?.text = nil
                return
            }
            textLabel?.text = comment.content
        }
    }
    override func prepareForReuse() {
        textLabel?.text = nil
    }
}
