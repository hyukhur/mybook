//
//  ResponseError.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/13.
//

import Foundation

struct ResponseError: Codable {
    let error: String
}
