//
//  BookTableViewCell.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/12.
//

import UIKit

extension Book {
    func openURL() {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}

class BookTableViewCell: UITableViewCell {
    static let identifier: String = "BookTableViewCell"

    @MainActor
    var book: Book? {
        didSet {
            // TODO: It could be replaced with a placeholder
            if #available(iOS 13.0, *) {
                imageURLView.image = UIImage(systemName: "photo")
            }
            guard let book = book else {
                titleLabel.text = nil
                subtitleLabel.text = nil
                priceLabel.text = nil
                return
            }
            if oldValue != book {
                titleLabel.text = book.title
                subtitleLabel.text = book.subtitle
                priceLabel.text = book.price
                imageURLView.load(url: book.image)
            }
        }
    }

    let titleLabel: UILabel = UILabel()
    let subtitleLabel: UILabel = UILabel()
    let priceLabel: UILabel = UILabel()
    let imageURLView: UIImageView = UIImageView()
    let urlButton: UIButton = UIButton(type: .infoDark)

    lazy var textStackView: UIStackView = {
        let vStackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel, priceLabel])
        vStackView.axis = .vertical
        vStackView.distribution = .equalSpacing
        vStackView.alignment = .leading
        vStackView.spacing = 6
        return vStackView
    }()

    lazy var contentStackView: UIStackView = {
        let hStackView = UIStackView(arrangedSubviews: [imageURLView, textStackView, urlButton])
        hStackView.axis = .horizontal
        hStackView.distribution = .fillProportionally
        hStackView.alignment = .firstBaseline
        hStackView.spacing = 4
        hStackView.layoutMargins = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        hStackView.translatesAutoresizingMaskIntoConstraints = false
        return hStackView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.addSubview(contentStackView)
        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: contentStackView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: contentStackView.trailingAnchor),
            contentView.topAnchor.constraint(equalTo: contentStackView.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: contentStackView.bottomAnchor),
            imageURLView.heightAnchor.constraint(equalToConstant: 88),
            imageURLView.widthAnchor.constraint(equalToConstant: 88),
            urlButton.widthAnchor.constraint(equalToConstant: 44)
        ])

        titleLabel.numberOfLines = 0
        subtitleLabel.numberOfLines = 0
        urlButton.addTarget(self, action: #selector(urlButtonTapped(_:)), for: .touchUpInside)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        book = nil
    }

    @objc
    func urlButtonTapped(_ sender: UIButton?) {
        book.map {
            $0.openURL()
        }
    }
}
