//
//  BookDetailView.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/17.
//

import UIKit

class BookDetailView: UIView {
    lazy var authorsLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: UIFont.labelFontSize)
        return label
    }()
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    lazy var ratingLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    lazy var imageURLView: UIImageView = {
        let imageView = UIImageView()
        imageView.widthAnchor.constraint(equalToConstant: frame.size.width).isActive = true
        imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
        return imageView
    }()
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    lazy var isbn10Label: UILabel = {
        let label = UILabel()
        return label
    }()
    lazy var isbn13Label: UILabel = {
        let label = UILabel()
        return label
    }()
    lazy var pagesLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    lazy var languageLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    lazy var publisherLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    lazy var yearLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    lazy var urlTextView: UITextView = {
        let textView = UITextView()
        textView.isUserInteractionEnabled = true
        textView.isEditable = false
        textView.dataDetectorTypes = .link
        textView.linkTextAttributes = [.foregroundColor: UIColor.systemBlue]
        return textView
    }()

    lazy var contentStackView: UIStackView = {
        let vStackView = UIStackView(arrangedSubviews: [authorsLabel, titleLabel, subtitleLabel, ratingLabel, imageURLView, priceLabel, isbn10Label, isbn13Label, pagesLabel, languageLabel, publisherLabel, yearLabel, urlTextView])
        vStackView.axis = .vertical
        vStackView.translatesAutoresizingMaskIntoConstraints = false
        return vStackView
    }()

    var book: BookDetail? {
        didSet {
            guard let book = book else { return }
            guard book != oldValue else { return }
            authorsLabel.text = book.authors
            titleLabel.text = book.title
            subtitleLabel.text = book.subtitle
            ratingLabel.text = book.rating
            imageURLView.load(url: book.image)
            priceLabel.text = book.price
            isbn10Label.text = book.isbn10
            isbn13Label.text = book.isbn13
            pagesLabel.text = book.pages
            languageLabel.text = book.language
            publisherLabel.text = book.publisher
            yearLabel.text = book.year
            urlTextView.text = book.url
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(contentStackView)
        NSLayoutConstraint.activate([
            self.leadingAnchor.constraint(equalTo: contentStackView.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: contentStackView.trailingAnchor),
            self.topAnchor.constraint(equalTo: contentStackView.topAnchor),
            self.bottomAnchor.constraint(equalTo: contentStackView.bottomAnchor),
            urlTextView.heightAnchor.constraint(equalToConstant: 44)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
