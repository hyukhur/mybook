//
//  UIImageView+Remote.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/13.
//

import UIKit

protocol ImageCache {
    func image(for: URL) -> UIImage?
    func set(image: UIImage, for: URL)
}

// TODO: DiskCache and a composit cache would be introduced later
struct MemoryCache: ImageCache {
    var cache: NSCache<NSURL, UIImage> = NSCache()
    func image(for url: URL) -> UIImage? {
        cache.object(forKey: url as NSURL)
    }

    func set(image: UIImage, for url: URL) {
        cache.setObject(image, forKey: url as NSURL)
    }
}

extension UIImageView {
    typealias AsyncJob = (_ group: DispatchGroup?, _ qos: DispatchQoS, _ flags: DispatchWorkItemFlags, _ execute: @escaping @convention(block) () -> Void) -> Void
    static var bgAsyncJob: AsyncJob = DispatchQueue.global().async
    static var mainAsyncJob: AsyncJob = DispatchQueue.main.async

    struct RemoteLoadOptions {
        var loader : (URL) throws -> Data =  {
            try Data(contentsOf: $0)
        }
        var decoder: (Data) throws -> UIImage? = {
            UIImage(data: $0)
        }
        var cache: ImageCache = MemoryCache()
    }

    static var remoteLoadOptions: RemoteLoadOptions = RemoteLoadOptions()

    // TODO: We could return a token to cancel task.
    func load(url: URL, options: RemoteLoadOptions = UIImageView.remoteLoadOptions, completion: @escaping (UIImage) -> UIImage? = { $0 } ) {
        dispatchPrecondition(condition: .onQueue(.main))
        if let cachedImage = options.cache.image(for: url) {
            image = completion(cachedImage)
            return
        }
        // TODO: bgAsyncJob's parameters could be optionalized
        Self.bgAsyncJob(nil, .unspecified, []) { [weak self] in
            do {
                let data = try options.loader(url)

                guard let image = try options.decoder(data) else {
                    return
                }

                options.cache.set(image: image, for: url)
                // TODO: mainAsyncJob's parameters could be optionalized
                Self.mainAsyncJob(nil, .unspecified, []) {
                    self?.image = completion(image)
                }
            } catch {
                // TODO: error handler
            }
        }
    }
}
