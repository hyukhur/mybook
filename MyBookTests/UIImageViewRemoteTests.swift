//
//  UIImageViewRemoteTests.swift
//  MyBookTests
//
//  Created by hyukhur on 2022/02/13.
//

import XCTest
@testable import MyBook

class UIImageViewRemoteTests: XCTestCase {
    let url = URL(string: "https://itbook.store/img/books/9781484206485.png")!
    let placeholderImage = UIImage(systemName: "photo")!

    override func setUpWithError() throws {
        UIImageView.bgAsyncJob = { _, _, _, execute in execute() }
        UIImageView.mainAsyncJob = { _, _, _, execute in execute() }
    }

    func testLoadImageFromURL() {
        let imageView = UIImageView()
        XCTAssertEqual(imageView.image, nil)

        var resultImage: UIImage? = placeholderImage
        imageView.load(url: url) {
            resultImage = $0
            return $0
        }

        XCTAssertNotEqual(imageView.image, nil)
        XCTAssertEqual(imageView.image, resultImage)
    }

    func testLoadResizedImageFromURL() {
        let imageView = UIImageView()
        XCTAssertEqual(imageView.image, nil)

        var resultImage: UIImage? = placeholderImage
        imageView.load(url: url) {
            resultImage = $0
            return $0.resizableImage(withCapInsets: UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1))
        }

        XCTAssertNotEqual(imageView.image, nil)
        XCTAssertNotEqual(imageView.image, resultImage)
    }

    func testLoadRemoteImageWithPlaceholder() {
        let imageView = UIImageView()
        imageView.image = placeholderImage
        XCTAssertNotEqual(imageView.image, nil)
        XCTAssertEqual(imageView.image, placeholderImage)

        var resultImage: UIImage?
        imageView.load(url: url) {
            resultImage = $0
            return $0
        }

        XCTAssertNotEqual(imageView.image, placeholderImage)
        XCTAssertEqual(imageView.image, resultImage)
    }

    func testLoadLocalImage() {
        let imageView = UIImageView()
        XCTAssertEqual(imageView.image, nil)

        let options = UIImageView.RemoteLoadOptions(loader: { _ in
            self.placeholderImage.pngData()!
        })

        imageView.load(url: url, options: options)

        XCTAssertNotEqual(imageView.image, nil)
        // Binaries are same
        XCTAssertEqual(imageView.image?.pngData(), UIImage(data: placeholderImage.pngData()!)?.pngData())
    }

    func testCustomImageDecoder() {
        let imageView = UIImageView()
        XCTAssertEqual(imageView.image, nil)

        let options = UIImageView.RemoteLoadOptions(decoder: { _ in
            self.placeholderImage
        })

        imageView.load(url: url, options: options)

        XCTAssertNotEqual(imageView.image, nil)
        XCTAssertEqual(imageView.image, placeholderImage)
    }

    func testImageCache() {
        let imageView = UIImageView()
        XCTAssertEqual(imageView.image, nil)
        XCTAssertEqual(UIImageView.remoteLoadOptions.cache.image(for: url), nil)

        imageView.load(url: url)
        XCTAssertNotEqual(imageView.image, nil)
        XCTAssertEqual(imageView.image, UIImageView.remoteLoadOptions.cache.image(for: url))

        let downloadedImage = imageView.image

        UIImageView.remoteLoadOptions.cache.set(image: placeholderImage, for: url)
        imageView.load(url: url)
        XCTAssertNotEqual(imageView.image, nil)
        XCTAssertEqual(UIImageView.remoteLoadOptions.cache.image(for: url), placeholderImage)
        XCTAssertNotEqual(imageView.image, downloadedImage)
        XCTAssertEqual(imageView.image, placeholderImage)
    }
}
