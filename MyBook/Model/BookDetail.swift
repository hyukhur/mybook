//
//  BookDetail.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/13.
//

import Foundation

struct BookDetail: Decodable, Equatable {
    let authors: String
    let desc: String
    let error: String
    let image: URL
    let isbn10: String
    let isbn13: String
    let language: String?
    let pages: String
    let price: String
    let publisher: String
    let rating: String
    let subtitle: String
    let title: String
    let url: String
    let year: String
    //    {
    //        "Chapter 2": "https://itbook.store/files/9781617294136/chapter2.pdf",
    //        "Chapter 5": "https://itbook.store/files/9781617294136/chapter5.pdf"
    //    }
    let pdf: [String: String]?

    var id: String {
        isbn13
    }

    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.id == rhs.id
    }
}
