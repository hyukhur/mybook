//
//  DetailViewController.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/12.
//

import UIKit

class DetailViewController: UIViewController {
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: self.view.bounds)
        tableView.delegate = self
        tableView.dataSource = self
        // TODO: Make sure it's the latest way.
        tableView.register(BookDetailTableViewCell.self, forCellReuseIdentifier: BookDetailTableViewCell.identifier)
        tableView.register(BookDetailCommentInputView.self, forHeaderFooterViewReuseIdentifier: BookDetailCommentInputView.identifier)
        return tableView
    }()

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl: UIRefreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "ViewController.refreshControl"))
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        return refreshControl
    }()

    lazy var detailView: BookDetailView = BookDetailView(frame: view.bounds)

    var isbn13: String = ""
    var book: BookDetail? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.reloadHeaderView()
            }
        }
    }

    var comments: [Comment] = []

    var apiClient: API?

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.addSubview(refreshControl)
        tableView.tableHeaderView = detailView
        view.addSubview(tableView)

        refresh(nil)

        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { [weak self] in
            if let height = ($0.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
                self?.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: height, right: 0)
            } else {
                self?.tableView.contentInset = .zero
            }
        }

        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) { [weak self] _ in
            self?.tableView.contentInset = .zero
        }
    }
}

extension DetailViewController {
    @objc
    func refresh(_ sender: Any?) {
        refreshControl.beginRefreshing()
        fetch() { [weak self] in
#if DEBUG
            print($0)
#endif
            guard let `self` = self else { return }
            defer {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
            self.book = (try? $0.get())
        }
    }

    func fetch(completion: @escaping (Result<BookDetail, Error>) -> Void) {
        apiClient?.get(to: EndPoint.books(isbn13: isbn13), completion: completion)
    }

    func reloadHeaderView() {
        detailView.book = book
        let height = detailView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        if height != detailView.frame.height {
            detailView.frame = CGRect(x: detailView.frame.origin.x, y: detailView.frame.origin.y, width: detailView.frame.size.width, height: height)
            tableView.tableHeaderView = detailView
        }
    }
}

extension DetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        comments.count
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: BookDetailCommentInputView.identifier) as? BookDetailCommentInputView
        view?.textField.delegate = self
        return view
    }
}

extension DetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // TODO: would be a factory method
        let cell = tableView.dequeueReusableCell(withIdentifier: BookDetailTableViewCell.identifier, for: indexPath) as? BookDetailTableViewCell ?? BookDetailTableViewCell(style: .default, reuseIdentifier: BookDetailTableViewCell.identifier)
        cell.comment = comments[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension DetailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text, !text.isEmpty {
            let newIndexPath = IndexPath(row: comments.count, section: 0)
            let comment = Comment(uuid: UUID(), content: text)
            comments.append(comment)
            tableView.beginUpdates()
            tableView.insertRows(at: [newIndexPath], with: .automatic)
            tableView.endUpdates()

            DispatchQueue.main.async {
                self.tableView.scrollToRow(at: newIndexPath, at: .bottom, animated: true)
            }
        }
        textField.text = nil
        textField.resignFirstResponder()
        return true
    }
}
