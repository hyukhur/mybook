//
//  EndPoint.swift
//  MyBook
//
//  Created by hyukhur on 2022/02/13.
//

import Foundation

enum EndPoint {
    static var baseURL: URL = URL(string: "https://api.itbook.store/1.0")!
    // https://api.itbook.store/1.0/search/{query}/{page}
    case search(query: String, page: String = "1") // TODO: check the default value of page
    // https://api.itbook.store/1.0/books/{isbn13}
    case books(isbn13: String)

    var path: String {
        switch self {
            case .search(_ , _):
                return "search"
            case .books(_):
                return "books"
        }
    }
}

extension EndPoint {
    var url: URL {
        switch self {
            case .search(query: "", page: _):
                return Self.baseURL
                    .appendingPathComponent(path)
            case .search(let query, page: ""):
                return Self.baseURL
                    .appendingPathComponent(path)
                    .appendingPathComponent(query)
            case .search(let query, let page):
                return Self.baseURL
                    .appendingPathComponent(path)
                    .appendingPathComponent(query)
                    .appendingPathComponent(page)
            case .books(let isbn13):
                return Self.baseURL
                    .appendingPathComponent(path)
                    .appendingPathComponent(isbn13)
        }
    }
}

